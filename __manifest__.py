# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Formato de Factura SoftnetCorp',
    'version' : '1.0.0.0',
    'summary': 'Imprime factura con formato libre de SoftnetCorp',
    
    'description': """

    """,
    'author': 'Jose Colmenares',
    'collaborator': 'Jose Colmenares',
    'category': 'Facturas',
    'website': 'https://www.odoo.com/page/billing',
    'depends' : ['base','account'],
    'data': [
        
        'report/template_invoice_print.xml',
        'report/invoice_print.xml',
        'report/button_view.xml'
    ],
    
    'installable': True,
    'application': True,
    'auto_install': False,
  
}
